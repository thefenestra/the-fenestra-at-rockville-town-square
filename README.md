The Fenestra at Rockville Town Square is a suburban sanctuary surrounded by modern urban style. Centered at the heart of Rockville Square, our three mid-rise buildings include spacious one, two and three bedroom residences within steps of so many modern conveniences—boutique shopping and dining.

Address: 20 Maryland Avenue, Rockville, MD 20850, USA

Phone: 301-279-0999
